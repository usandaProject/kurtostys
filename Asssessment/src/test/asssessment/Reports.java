/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.asssessment;

import static test.asssessment.DataDrivenClass.TestCase;
import static test.asssessment.SeleniumWebdriver.TakeScreenShot;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

/**
 *
 * @author Usanda Mpengesi
 */
public class Reports {

    String File;
    public static String FileDir = "";
    public static String dir = "";
    public static String actualFile ="";

    public Reports(String fileLocation) {
        this.File = fileLocation;
    }

    public void createReportFile() {
        try {
            actualFile =FileDir+"\\"+File + ".txt";
            File f = new File(actualFile);
            
            f.createNewFile();
        } catch (IOException e) {
        }
    }

    public void createDir() {

        FileDir = "ReportsEvidence\\" + File ;

        Path dir = Paths.get("ReportsEvidence\\" + File);

        if (!Files.exists(dir)) {
            try {
                Files.createDirectories(dir);
                createReportFile();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void FailPass_Results(String TestMessage, String Status) {
        try {
            TakeScreenShot(FileDir);
            BufferedWriter writer = new BufferedWriter(new FileWriter(actualFile, true));

            writer.write("Method Runing :" + TestCase + "\t\t" + TestMessage + "\t\t" + Status);

            writer.newLine();
            writer.close();

        } catch (IOException e) {
            System.out.println("Could not write to file.");
        }

    }

}
