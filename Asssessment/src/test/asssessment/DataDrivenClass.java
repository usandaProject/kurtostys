/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.asssessment;

import static test.asssessment.SeleniumWebdriver.DriverClose;
import test.asssessment.TestCaseClass.Login;
import test.asssessment.TestCaseClass.NavigateToApi;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author Usanda Mpengesi
 */
public class DataDrivenClass {

    FileInputStream fs;
    int numberOfRws = 0;
    static Sheet sh;
    static Workbook kywrd;
    static int totalNoOfRows;
    static int totalNoOfCols;
    static int KeepTrackOfrow = 0;
    String StringData;
    String excelpath = "";
    static String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    Reports report;
    public static String TestCase= ""; 

    public DataDrivenClass(String filePath) throws FileNotFoundException, IOException, BiffException {
        fs = new FileInputStream(filePath);
        kywrd = Workbook.getWorkbook(fs);
        sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();
        this.StringData = filePath;
        this.report = new Reports("ExecutionReport_" + currentTime);

    }

    public void RunTest() throws IOException, BiffException {
        report.createDir();

        try {

            int rowcounter = 0;
            while (rowcounter < totalNoOfRows) {

                String Keyword = sh.getCell(0, rowcounter).getContents();//getting contents of rows 
                TestCase=Keyword;
                switch (Keyword) {

                    case "Navigate to WebTables":
                        NavigateToApi Navigate = new NavigateToApi(StringData);
                        Navigate.ExecuteNavigate();
                        break;

                    case "AddUser":
                        Login login = new Login(StringData);
                        login.ExecuteLogin();
                        break;

                }

                rowcounter = rowcounter + 2;
                KeepTrackOfrow = rowcounter;
            }
            DriverClose();
        } catch (Exception e) {
            System.err.println("<Error> "+e);
        }

    }

    public String getData(String Find) throws IOException, BiffException {

        try {

            for (int col = 1; col < totalNoOfCols; col++) {

                String ColumnValue = sh.getCell(col, KeepTrackOfrow).getContents();//getting contents of rows 
                if (Find.equalsIgnoreCase(ColumnValue)) {

                    return sh.getCell(col, KeepTrackOfrow + 1).getContents();

                }

            }

        } catch (Exception e) {
            System.err.println("Fialed to Find " + Find + " value");
        }
        return "";
    }

}
