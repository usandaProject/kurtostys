/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.asssessment.TestCaseClass;

import test.asssessment.DataDrivenClass;
import test.asssessment.Instances;
import static test.asssessment.Reports.FailPass_Results;
import test.asssessment.XpathClass;
import java.io.FileNotFoundException;
import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Usanda Mpengesi
 */
public class NavigateToApi extends Instances {

    DataDrivenClass Data;

    public NavigateToApi(String Path) throws IOException, FileNotFoundException, BiffException {
        this.Data = new DataDrivenClass(Path);

    }

    public boolean ExecuteNavigate() throws IOException, BiffException, InterruptedException {
        if (!Navigate2WebTables()) {
           
            return false;
        }
        return true;
    }

    public boolean Navigate2WebTables() throws IOException, BiffException, InterruptedException {
        if (!SeleniumActions.Navigate2Browser("Chrome", Data.getData("Enviroment"))) {
            return false;
        }

        return true;
    }

}
