/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.asssessment.TestCaseClass;

import test.asssessment.DataDrivenClass;
import test.asssessment.Instances;
import static test.asssessment.Reports.FailPass_Results;
import test.asssessment.XpathClass;
import java.io.FileNotFoundException;
import java.io.IOException;
import jxl.read.biff.BiffException;

/**
 *
 * @author Siphamandla
 */
public class Login extends Instances {

    DataDrivenClass Data;

    public Login(String Path) throws IOException, FileNotFoundException, BiffException {

        this.Data = new DataDrivenClass(Path);

    }

    /**
     *
     * @return @throws IOException
     * @throws BiffException
     * @throws InterruptedException
     */
    public boolean ExecuteLogin() throws IOException, BiffException, InterruptedException {

        if (!TestFlow()) {
            FailPass_Results(Data.getData("Username") + " not added Succesfully.", "Failed");
            return false;
        }

        return true;
    }

    public boolean TestFlow() throws IOException, BiffException {
        if (!SeleniumActions.clickElement(XpathClass.disclaimer())) {
            return false;
        }

        if (!SeleniumActions.clickElement(XpathClass.featuredFunds())) {
            return false;
        }

        if (!SeleniumActions.clickElement(XpathClass.findOutMore())) {
            return false;
        }
        if (!SeleniumActions.clickElement(XpathClass.placeholder())) {
            return false;
        }

        if (!SeleniumActions.clickElement(XpathClass.seachbox())) {
            return false;
        }

        if (!SeleniumActions.EnterByXpaths(XpathClass.seachbox(), Data.getData("value"))) {
            return false;
        }

  if (!SeleniumActions.clickElement(XpathClass.placeholderDropdown())) {
            return false;
        }
        if (SeleniumActions.WaitbyXpath(XpathClass.isin(), 3)) {
            FailPass_Results(Data.getData("value") + " Match ", "Pass");
        } else {
            FailPass_Results(Data.getData("value") + " does not Match ", "Fail");
            return false;
        }

        if (!SeleniumActions.clickElement(XpathClass.perfomanceFee())) {
            return false;
        }
           if (!SeleniumActions.WaitbyXpath(XpathClass.graph(), 4)){
            return false;
        }

        FailPass_Results(Data.getData("Username") + " Successfully logged in", "Pass");
        return true;
    }

}
