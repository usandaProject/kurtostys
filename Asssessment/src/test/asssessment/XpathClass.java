/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.asssessment;

/**
 *
 * @author Usanda Mpengesi
 */
public class XpathClass {

    public static String disclaimer() {
        return "//div[@class='btn btn-cta ng-binding']";
    }

    public static String featuredFunds() {
        return "//a[text()='VIEW OUR FEATURED FUNDS']";
    }

    public static String findOutMore() {
        return "//div[text()='JPM UK Dynamic Fund']//../../../..//span[text()='Find out more']";
    }

    public static String placeholder() {
        return "//div[@id='jump-to-fund-placehoder']";
    }

    public static String placeholderDropdown() {
        return  "//div[@class='row margin-horizontal-0']";
    }
    
   
    
    public static String seachbox() {
        return "//input[@id='searchbox']";
    }

    public static String dropdown() {
        return "//span[text()='JPM A - Net Acc - GBP']";
    }

    public static String perfomanceFee() {
        return "//span[text()='PERFORMANCE & FEES'][2]";
    }

    public static String isin() {
        return "//span[text()='GB0009698001'][2]";
    }

    public static String graph() {
        return "//img[@id='featured-image']";
    }

}
